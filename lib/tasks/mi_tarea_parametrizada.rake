require 'rake'

desc 'Esta tarea suma los numeros recibidos'

task :sumar, :un_numero, :otro_numero do |task, args|
  un_numero = args[:un_numero].to_i
  otro_numero = args[:otro_numero].to_i

  # hago cosas con mis parametros
  puts un_numero + otro_numero
end
